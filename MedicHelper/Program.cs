﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using API;
using Newtonsoft.Json;
using WindowsInput;
namespace MedicHelper
{
    class Day
    {
        public List<AmbulanceCall> Calls = new List<AmbulanceCall>();
        public int HealedCount;
        public DateTime date;
    }
    struct Person
    {
        public string Name;
        public int Id;
    }
    struct AmbulanceCall
    {
        public Person Caller;
        public double Distance;
        public bool Accepted;
    }
    class Program
    {
        static Chat Chat;
        static Game Game;
        static Player Player;
        static World World;
        static Vehicle Vehicle;
        static RemotePlayer remotePlayer;
        static InputSimulator sim;
        static ZoneManager zoneManager;
        static Timer TimerReport;
        static List<AmbulanceCall> AmbulanceCalls;
        static int HealedCount = 0;
        static string MyName;
        static void Main(string[] args)
        {
            sim = new InputSimulator();
            zoneManager = new ZoneManager();
            int initResult = API.API.Init();
            if (initResult > 0)
            {
                Console.WriteLine("SAMP обнаружен");
                Chat = Chat.GetInstance();
                Game = Game.GetInstance();
                Player = Player.GetInstance();
                World = World.GetInstance();
                Vehicle = Vehicle.GetInstance();
                remotePlayer = RemotePlayer.GetInstance();
                AmbulanceCalls = new List<AmbulanceCall>();
                Chat.OnChatMessage += OnChatMessageReceived;
                Chat.OnCommand += OnCommandReceived;
                MyName = Player.GetName();
                Chat.RegisterCommand("/ssf", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 1");
                    Chat.AddMessage("Спавн изменен на центральную больницу Сан-Фиерро [SFMC]");
                });
                Chat.RegisterCommand("/slsg", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 2");
                    Chat.AddMessage("Спавн изменен на больницу Лос-Сантос в гетто [CGH]");
                });
                Chat.RegisterCommand("/sls", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 3");
                    Chat.AddMessage("Спавн изменен на центральную больницу Лос-Сантос [ASGH]");
                });
                Chat.RegisterCommand("/slslv", (c, v) =>
                {
                    Chat.Send("/spawnchange 4");
                    Chat.AddMessage("Спавн изменен на  Crippen Memorial Hospital (деревня у ЛС-ЛВ)");
                });
                Chat.RegisterCommand("/slv", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 5");
                    Chat.AddMessage("Спавн изменен на Las Venturas Hospital");
                });
                Chat.RegisterCommand("/sfc", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 6");
                    Chat.AddMessage("Спавн изменен на Fort Carson Medical Center [Больница д,.Fort Carson]");
                });
                Chat.RegisterCommand("/slvel", (c, v) =>
                {
                    Thread.Sleep(1100);
                    Chat.Send("/spawnchange 6");
                    Chat.AddMessage("Спавн изменен на El Quebrados Medical Center [Деревня около г. Las Venturas]");
                });
                Chat.RegisterCommand("/spawns", (c, v) =>
                {
                    Chat.AddMessage("НАЗВАНИЯ И НОМЕРА БОЛЬНИЦ");
                    Chat.AddMessage("[1] - San Fierro Medical Center [Больница г. San-Fierro] [/ssf]");
                    Chat.AddMessage("[2] - County General Hospital [Гетто] [/slsg]");
                    Chat.AddMessage("[3] - All Saints General Hospital [Больница г. Los-Santos] [/sls]");
                    Chat.AddMessage("[4] - Crippen Memorial Hospital [Деревня около г. Los Santos] [/slslv]");
                    Chat.AddMessage("[5] - Las Venturas Hospital [Больница г. Las-Venturas] [/slv]");
                    Chat.AddMessage("[6] - Fort Carson Medical Center [Больница д,.Fort Carson] [/sfc]");
                    Chat.AddMessage("[7] - El Quebrados Medical Center [Деревня около г. Las Venturas] [/slvel]");
                });
                Chat.RegisterCommand("/makereport", (c, v) =>
                {
                    MakeReport(false);
                });

                Chat.RegisterCommand("/makeadvreport", (c, v) =>
                {
                    MakeReport(true);
                });
                Chat.RegisterCommand("/timereport", (c, v) =>
                {
                    TimeReport(c, v);
                });
                Chat.RegisterCommand("/aac", OnServiceAccepted);
                Chat.RegisterCommand("/aacc", OnCustomServiceAccepted);
                Chat.RegisterCommand("/mh", (c, v) =>
                {
                    Chat.AddMessage("MedicHelper is working!", "f56342");
                });
                Chat.RegisterCommand("/gs", (c, v) =>
                {
                    Chat.AddMessage($"Маркер находится в {GetCurrentCity()}, {GetCurrentZone()}");
                });
                Chat.RegisterCommand("/mhstats", (c, v) =>
                {
                    var ds = GetAllDays();
                    if (ds != null)
                    {
                        int totalHealedCount = 0;
                        int totalCallAccepted = 0;
                        foreach (var day in ds)
                        {
                            totalHealedCount += day.HealedCount;
                            totalCallAccepted += day.Calls.Count;
                        }
                        string report = $"Всего принято вызовов: {totalCallAccepted} вылечено: {totalHealedCount}";
                        Chat.AddMessage(report);
                        Console.WriteLine(report);
                    }
                });
                Chat.AddMessage("MedicHelper by FoxPro [Dima_Galkin] loaded sucessfully!", "f56342");
                Chat.AddMessage("/mh - статус программы, /gs - текущая зона и город, /aac - принять последний вызов, /aacc [id] - принять вызов от id", "f56342");
                Chat.AddMessage("/spawns - список больниц, /mhstats - статистика вызовов", "f56342");
                var days = GetAllDays();
                if (days != null)
                {
                    int totalHealedCount = 0;
                    int totalCallAccepted = 0;
                    foreach (var day in days)
                    {
                        if (day.date.Date == DateTime.Now.Date)
                        {
                            AmbulanceCalls = day.Calls;
                            HealedCount = day.HealedCount;
                        }
                        totalHealedCount += day.HealedCount;
                        totalCallAccepted += day.Calls.Count;
                    }
                    string report = $"Всего принято вызовов: {totalCallAccepted} вылечено: {totalHealedCount}";
                    Chat.AddMessage(report);
                    Console.WriteLine(report);
                }
            }
            Console.ReadKey();
        }

        private static void TimeReport(string command, string[] args)
        {
            if (args.Length == 0)
            {
                Chat.AddMessage("Введите параметры [on/off] [время в минутах]");
                return;
            }
            else if (args.Length == 1)
            {
                if (args[0] == "off")
                {
                    if (TimerReport != null)
                        TimerReport.Dispose();
                    Chat.AddMessage("Таймер отчетов выключен");
                    return;
                }
                else
                {
                    Chat.AddMessage("Введите параметры [on/off] [время в минутах]");
                    return;
                }
            }

            string onoff = args[0];
            int period = int.Parse(args[1]);
            if (onoff == "on")
            {
                TimerReport = new Timer((state) => MakeReport(), null, 5000, period * 1000 * 60);
                Chat.AddMessage($"Установлен таймер на {period} миунт");
            }
        }

        private static void MakeReport(bool advanced = false)
        {
            Thread.Sleep(1100);
            Chat.Send($"/f {DateTime.Now.AddHours(-2).ToString("HH:mm")} | Патруль | Вызовов: {AmbulanceCalls.Count} | Вылечено: {HealedCount}");
            Thread.Sleep(1100);
            Chat.Send($"/f Местоположение: {GetCurrentCity()}, {GetCurrentZone()}");
            if (advanced)
            {
                Thread.Sleep(1250);
                if (AmbulanceCalls.Count == 0) return;
                Chat.Send($"/f Приняты вызовы от:");
                Thread.Sleep(1250);
                string threeNames = "";
                for (int i = 0; i < AmbulanceCalls.Count; i++)
                {
                    if (!AmbulanceCalls[i].Accepted) continue;
                    if (i == AmbulanceCalls.Count - 1)
                        threeNames += AmbulanceCalls[i].Caller.Name.Replace("_", " ");
                    else
                        threeNames += AmbulanceCalls[i].Caller.Name.Replace("_", " ") + ", ";
                    if (i % 3 == 0 || i == AmbulanceCalls.Count - 1)
                    {
                        Chat.Send($"/f {threeNames}");
                        threeNames = "";
                        Thread.Sleep(1250);
                    }
                }
            }
        }

        static List<Day> GetAllDays()
        {
            if (!File.Exists("stats.txt")) return null;
            return JsonConvert.DeserializeObject<List<Day>>(File.ReadAllText("stats.txt"));
        }
        static void SaveAmbulanceCall(AmbulanceCall call)
        {
            if (!File.Exists("stats.txt"))
            {
                FileStream fs = File.Create("stats.txt");
                fs.Close();
            }
            List<Day> days = JsonConvert.DeserializeObject<List<Day>>(File.ReadAllText("stats.txt"));
            if (days == null) days = new List<Day>();
            int ind = days.FindIndex(x => x.date.Date == DateTime.Now.Date);
            if (ind > -1)
            {
                if (HealedCount > days[ind].HealedCount)
                    days[ind].HealedCount = HealedCount;
                days[ind].Calls.Add(call);
            }
            else
            {
                Day day = new Day
                {
                    Calls = AmbulanceCalls,
                    date = DateTime.Now,
                    HealedCount = HealedCount
                };
                days.Add(day);
            }
            File.WriteAllText("stats.txt", JsonConvert.SerializeObject(days));
        }
        private static void SaveHealedCount()
        {
            if (!File.Exists("stats.txt"))
            {
                FileStream fs = File.Create("stats.txt");
                fs.Close();
            }
            List<Day> days = JsonConvert.DeserializeObject<List<Day>>(File.ReadAllText("stats.txt"));
            if (days == null) days = new List<Day>();
            int ind = days.FindIndex(x => x.date.Date == DateTime.Now.Date);
            if (ind > -1)
            {
                if (HealedCount > days[ind].HealedCount)
                    days[ind].HealedCount = HealedCount;
            }
            else
            {
                Day day = new Day
                {
                    Calls = AmbulanceCalls,
                    date = DateTime.Now,
                    HealedCount = HealedCount
                };
                days.Add(day);
            }
            File.WriteAllText("stats.txt", JsonConvert.SerializeObject(days));
        }
        static string GetCurrentCity(Position pos = null)
        {
            if (pos == null) pos = new Position(Player.GetX(), Player.GetY(), Player.GetZ());
            return zoneManager.City(pos.X, pos.Y, pos.Z);
        }
        static string GetCurrentZone(Position pos = null)
        {
            if (pos == null) pos = new Position(Player.GetX(), Player.GetY(), Player.GetZ());
            return zoneManager.Zone(pos.X, pos.Y, pos.Z);
        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        private static void OnCustomServiceAccepted(string command, string[] args)
        {
            Thread.Sleep(1100);
            if (args.Length == 0)
            {
                Chat.AddMessage("Введите id игрока, чтобы принять вызов");
                return;
            }
            int id = int.Parse(args[0]);
            AmbulanceCall call = new AmbulanceCall
            {
                Caller = new Person()
            };
            call.Caller.Id = id;
            Chat.Send($"/service ac medic {call.Caller.Id}");
            Thread.Sleep(1100);
            for (int i = Chat.ChatHistory.Count - 1; Chat.ChatHistory.Count > 12 ? i > Chat.ChatHistory.Count - 12 : i > 0; i--)
            {
                if (Chat.ChatHistory[i].Contains("Место помечено на карте красной меткой"))
                {
                    string[] words = Chat.ChatHistory[i].Split(' ');
                    call.Distance = double.Parse(words[words.Length - 2].Replace(".", ","));
                    call.Accepted = true;
                }
                else if (Chat.ChatHistory[i].Contains($"Диспетчер: {MyName} принял вызов от"))
                {
                    string[] words = Chat.ChatHistory[i].Split(' ');
                    string name = words[words.Length - 1].Split('[')[0];
                    call.Caller.Name = name;
                }
            }
            ProcessCall(call);
        }
        private static void OnServiceAccepted(string command, string[] args)
        {
            Thread.Sleep(1100);
            string sname = "";

            for (int i = Chat.ChatHistory.Count - 1; Chat.ChatHistory.Count > 30 ? i > Chat.ChatHistory.Count - 30 : i > 0; i--)
            {
                if (Chat.ChatHistory[i].Contains("Диспетчер: вызов от "))
                {
                    AmbulanceCall call = new AmbulanceCall
                    {
                        Caller = new Person()
                    };
                    string dispCallLine = Chat.ChatHistory[i];
                    string[] dispCallArgs = dispCallLine.Split(' ');

                    int startBracket = dispCallLine.IndexOf('[');
                    string sid = "";
                    for (int j = startBracket + 1; j < dispCallLine.Length; j++)
                    {
                        if (dispCallLine[j] == ']') break;
                        sid += dispCallLine[j];
                    }
                    call.Caller.Id = int.Parse(sid);

                    int k = startBracket - 1;
                    while (dispCallLine[k] != 'т')
                    {
                        sname += dispCallLine[k];
                        k--;
                    }

                    call.Caller.Name = Reverse(sname.Substring(0, sname.Length - 1));
                    bool x = false;
                    string sdist = dispCallArgs[dispCallArgs.Length - 2].Replace('.', ',');
                    if (sdist.Contains(",")) x = true;
                    double distance = double.Parse(sdist);

                    if (x) distance *= 1000;
                    call.Distance = distance;

                    Chat.Send($"/service ac medic {call.Caller.Id}");
                    call.Accepted = true;
                    ProcessCall(call);
                    return;
                }
            }
            Chat.AddMessage("Не найдено вызвов", "f56342");
        }
        static double CalculateTime(double distance)
        {
            return Math.Round(distance * 2 / 2700, 0);
        }
        private static void ProcessCall(AmbulanceCall call)
        {
            AmbulanceCalls.Add(call);
            SaveAmbulanceCall(call);
            Thread.Sleep(1170);
            var pos = World.GetCheckpointPos();
            string city = zoneManager.City(pos.X, pos.Y, pos.Z);
            string zone = zoneManager.Zone(pos.X, pos.Y, pos.Z);

            Chat.Send($"/f Принял вызов из {city} | Нахожусь в {Player.GetZone()}, {Player.GetCity()} | {call.Distance} м.");

            if (!Vehicle.IsSirenEnabled()) Vehicle.ToggleSirenState();

            Chat.AddMessage($"Вы приняли вызов от {call.Caller.Name} [{call.Caller.Id}]. Расстояние до него - {call.Distance}", "f56342");
            Chat.AddMessage($"Местоположение: {city}, {zone}", "f56342");
            Thread.Sleep(1210);
            Chat.Send($"/t {call.Caller.Id} Скорая уже в пути! Находимся в {Player.GetCity()}");
            Thread.Sleep(1210);
            double timeToPoint = CalculateTime(call.Distance);
            if (timeToPoint > 1)
                Chat.Send($"/t {call.Caller.Id} До {city}, {zone} доберемся за {timeToPoint} минуты!");
            else
            {
                if (timeToPoint == 0) Chat.Send($"/t {call.Caller.Id} До {city}, {zone} скоро доберемся!");
                else Chat.Send($"/t {call.Caller.Id} До {city}, {zone} доберемся за {timeToPoint} минуту!");
            }
            Thread.Sleep(1205);
            Chat.Send($"/t {call.Caller.Id} По возможности опишите проблему, чтобы мы были готовы!");
        }

        private static void OnCommandReceived(string command, params string[] args)
        {
            //Console.WriteLine("[Команда]: " + message);
        }

        static void OnChatMessageReceived(DateTime time, string message)
        {
            //Console.WriteLine(message);
            //Chat.AddMessage("Вы получили сообщение в чате: ");
            if (message.Contains($"Медик {MyName} вылечил"))
            {
                HealedCount++;
                SaveHealedCount();
            }
            else if (message.Contains("Пациент вылечен от болезни"))
            {
                HealedCount++;
                SaveHealedCount();
            }
            else if (message.Contains("Сеанс лечения от болезни"))
            {
                HealedCount++;
                SaveHealedCount();
            }
            else if (message.Contains("Вы вылечили пациента"))
            {
                HealedCount++;
                SaveHealedCount();
            }
        }

    }


}
